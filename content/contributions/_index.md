+++
title = "Contributions"
description = "In this page you see some of my contributions that I want to highlight"
+++

</br>

-------

**Table of Content:**
- [Software Development](#software-development)
- [Financial Help/Donation](#financial-help-donation)
- [Computational Donation](#computational-donation)
- [Community Contributions](#community-contributions)

-------


# Software Development

I contribute to projects through various git platforms including
<a class="tooltip" href="https://codeberg.org/mehrad" target="_blank" title="Codeberg">
      <iconify-icon data-icon="simple-icons:codeberg" style="height: 18px; width: 18px"></iconify-icon> Codeberg
      <span class="tooltiptext">My primary choice</span>
</a>,
<a href="https://github.com/mmahmoudian" target="_blank" title="GitHub">
      <iconify-icon data-icon="simple-icons:github" style="height: 18px; width: 18px"></iconify-icon> Github
</a>,
<a href="https://gitlab.com/mehrad" target="_blank" title="GitLab">
      <iconify-icon data-icon="fa-brands:gitlab" style="height: 18px; width: 18px"></iconify-icon> Gitlab
</a>, and
<a class="tooltip" href="https://bitbucket.org/mehrad_mahmoudian" target="_blank" title="Bitbucket">
      <iconify-icon data-icon="simple-icons:bitbucket" style="height: 18px; width: 18px"></iconify-icon> <strike>Bitbucket</strike>
      <span class="tooltiptext">Littered with Spammers. I'm avoiding it!</span></a>.
For Github, perhaps the easiest and most compact way to illustrate is:


<figure class="image is-flex is-justify-content-center is-align-items-center">
    <img src="https://github-readme-stats.vercel.app/api?username=mmahmoudian&show_icons=true&theme=dracula" style="max-width: 500px" alt="My github account stats">
</figure>


<!--![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=mmahmoudian&theme=dracula)-->


-------


# Financial Help/Donation

I regularly donate to multiple projects that I believe are important for the world:

### From [my Patreon profile](https://www.patreon.com/mmahmoudian) I donate to:

- **Software**
    - **[Joplin](https://www.patreon.com/joplin)**: A Free and Libre OpenSource notetaking app that put user in control of their data (alternative to Evernote, Micso$oft Notes, and etc.)
    - **[MultiMC](https://www.patreon.com/multimc)**: A Minecraft launcher that put user in control
    - **[Matrix.org](https://www.patreon.com/matrixdotorg)**: A federated and OpenSource communication platform and protocol
    - **[Nyxt](https://www.patreon.com/nyxt)**: A keyboard-oriented, extensible web-browser designed for power users. [The application](https://github.com/atlas-engineer/nyxt) has familiar key-bindings, is fully configurable and extensible in Lisp, and has powerful features for productive professionals.
    - **[Project Mage](https://www.patreon.com/projectmage)**: [This project](https://project-mage.org/) is an effort to build a power-user environment and a set of applications in Common Lisp.
    - **[FOSS Torrents](https://www.patreon.com/fosstorrents)**: Creates Torrents for Free and Open-Source Softwares (FOSS) [[website](https://fosstorrents.com/)]
    - <details><summary>Project that I stopped donating to</summary>
    
      - <i><s>**[PodcastAddict app](https://www.patreon.com/podcastaddict)**: Perhaps the best podcatcher app ever build by mankind</s></i> --> Unfortunately I had to stop after perhaps 5 years of donation as the dev is charging via Google Pay subscription, and the app is still not FLOSS. Shame!
      - <i><s>**[Kovid Goyal](https://www.patreon.com/kovidgoyal)**: The software developer behind [Calibre ebook manager](https://calibre-ebook.com/) and [Kitty terminal](https://sw.kovidgoyal.net/kitty/)</s></i> --> I haven't used any of his software in about 2 years, so after over 3 years of donating, I decided to allocate my donation elsewhere that matters to me and are relevant to my daily life.
      - <i><s>**[Minecraft Forge](https://www.patreon.com/LexManos)**: A free and open-source tool that allows players to install and run Minecraft mods.</s></i> --> this project haven't been updated since 2019 (5 years!)
      - <i><s>**[Lutris](https://www.patreon.com/lutris)**: An OpenSource gaming platform</s></i> --> I have been donating to this project for yearsm and yet I haven't used this software even once. I think it is time to allocate my money to other projects
        
    </details>
    
- **Content Creators**
    - **[Veritasium](https://www.patreon.com/veritasium)**: A YouTube channel that explains some scientific phenomenon (mostly physics) in an illustrative way
    - **[StringCast](https://www.patreon.com/StringCast)**: A Persian podcast that explains science and history of science in a very interesting ways
    - **[StatQuest](https://www.patreon.com/statquest)**: A YouTube channel explaining statistics and machine learning in simple language
    - **[SciShow](https://www.patreon.com/scishow)**: A YouTube channel explaining science to people without too much scientific jargon
    - <details><summary>Project that I stopped donating to</summary>
    
      - <i><s>**[Technology Connections](https://www.patreon.com/m/1205741/posts)**: He creates [YouTube videos](https://www.youtube.com/c/TechnologyConnections) with detailed explanation of how certain technologies work, from toaster and dish-washing machine, to lamps and heat-pumps. I believe his contents would help educating people as the explanations are in layman terms and visual.</s></i> --> I don't think he needs my financial support considering the volume of ads he get on his videos. Maybe later when I'm in better financial situation I restart the donation for the sake of greater good.
    
    </details>
    
- **Independent Developers**
    - **[Martin Owens](https://inkscape.org/~doctormo/)**: A "Free Software Freelance programmer who works on [Inkscape](https://inkscape.org)".
    
- **Entertainment**
    - **[Robyn Adele Anderson](https://www.patreon.com/robynadele/)**: Creating jazz cover for mostly famous musics. Her music is available on [YouTube](https://www.youtube.com/c/RobynAdeleAndersonOfficial) and [her website](https://robynadelemerch.com/)
    - **[World's Greatest Con](https://www.patreon.com/m/7035623/)**: A very well-made podcast that tells true stories of "history's greatest deceptions". You can listen to episodes [here](https://worldsgreatestcon.fireside.fm/) or other platforms.
    
- **Initiatives**
    - **[Linux Accessibility](https://www.patreon.com/linux_a11y/posts)**: "Creating Accessibility Support for Free Software". This is [an effort by Christian Hempfling et al.](https://www.patreon.com/posts/kdes-team-68079468)



### From [my Liberapay](https://en.liberapay.com/mehrad.mahmoudian/) account I donate to:

- **Platforms**
    - **[Fosstodon.org](https://en.liberapay.com/fosstodon/)**: My favorite Mastodon instance themed around Free [and libre] OpenSource Software
    - **[Liberapay](https://en.liberapay.com/Liberapay/)**: A non-profit organization subscription payment platform.
    - **[Codeberg.org](https://en.liberapay.com/codeberg/)**: A non-profit organization dedicated to build and maintain supporting infrastructure for the creation, collection, dissemination, and archiving of Free and Open Source Software
    - **[Disroot](https://liberapay.com/Disroot/)**: A platform providing online services based on principles of freedom, privacy, federation and decentralization. [[website](https://disroot.org/en/faq)]
    - <details><summary>Project that I stopped donating to</summary>
    
      - <i><s>**[fidonet.io](https://en.liberapay.com/joacim/)**: Run and host free services for the benefit of the open web from our own datacenter in Sweden</s></i> --> Discontinued project
      - <i><s>**[Snopyta.org](https://en.liberapay.com/Snopyta/)**: Runs online services based on freedom, privacy and decentralization.</s></i> --> after 3 years of support, suddently out of the blue the whole website went down. Of course I don't donate to a service that does not use the donations to not to keep the service up and running!
      
    </details>
    
- **Projects**
    - **[magit](https://liberapay.com/magit/)**: One of the best packages in Emacs that makes life much easier for working with git.
    - **[tmux](https://liberapay.com/tmux/)**: In my opinion Tmux is the best terminal multiplexer in existance. Tmux make wokring on remote machines easy, fast, and productive.
    - **[keepassxc](https://liberapay.com/keepassxc/)**: The only viable and fully FLOSS password local vault/manager.[[website](https://keepassxc.org/)][[git](https://github.com/keepassxreboot/keepassxc)]
    
- **Independent Developers**
    - **[Protesilaos (Πρωτεσίλαος) Stavrou](https://liberapay.com/protesilaos/)**: An Emacs geek. His approach to Emacs have helped me get more comfortable and honest with my usage and use-case. At the time of writing this [he is looking for a job](https://protesilaos.com/news/2022-11-12-job-status-update/) and I thought my super small donation can at least buy him a coffee. ([Website](https://protesilaos.com), [YouTube](https://www.youtube.com/c/ProtesilaosStavrou), [Git](https://git.sr.ht/~protesilaos/)).
    - **[Martin Owens](https://inkscape.org/~doctormo/)**: A "Free Software Freelance programmer who works on [Inkscape](https://inkscape.org)".
    - **[Patrick Goldinger](https://liberapay.com/patrickgold/)**: A student developer who mainly developes [FlorisBoard](https://github.com/florisboard/florisboard), a privacy respecting and modern soft-keyboard.
    - **[tom79](https://liberapay.com/tom79/)**: Developer of [Fedilab](https://fedilab.app/), [UntrackMe](https://framagit.org/tom79/nitterizeme), [TubeLab9](https://framagit.org/tom79/fedilab-tube),  [OpenMultiMaps](https://framagit.org/tom79/openmaps), and [FediPlan](https://plan.fedilab.app/).
    - **[Markus Fisch](https://liberapay.com/markusfisch/)**: Developer of [BinaryEye](https://github.com/markusfisch/BinaryEye), [ScreenTime](https://github.com/markusfisch/ScreenTime), [Libra](https://github.com/markusfisch/Libra) and many more Android apps.



### From [my Github](https://github.com/mmahmoudian?tab=sponsoring) account I donate to:

- **Independent Developers**
    - **[Martin Tournoij](https://github.com/arp242)**: The main developer of [the GoatCounter](https://goatcounter.com/) which is an OpenSource privacy-friendly web analytics as an alternative to Google Analytics which [I use on this very website](https://mehradai.goatcounter.com).

### From [my OpenCollective](https://opencollective.com/mehrad-mahmoudian) account I donate to:

- **[Manjaro Linux](https://manjaro.org/)**: An Arch-based Linux distro with some batteries included and some extra safety nets.
- 
- **[LocalCDN](https://www.localcdn.org/)**: "A web browser extension that emulates Content Delivery Networks to improve your online privacy. It intercepts traffic, finds supported resources locally, and injects them into the environment."
- <details><summary>Project that I stopped donating to</summary>
    
      - <i><s>**[Manyverse](https://www.manyver.se)**: A social network off the grid based on SSB protocol and P2P connection over local network on internet.</s></i> --> The lead dev changed and the new one have not made changes to the repo for over 5 months.
      
    </details>



### Direct recurrent donations:

- **[The Document Foundation](https://www.documentfoundation.org/)**: An independent self-governing meritocratic charitable Foundation which is home to [LibreOffice](https://www.libreoffice.org/) and [The Document Liberation Project](https://www.documentliberation.org/).
- **[KDE e.V](https://ev.kde.org/)**: KDE e.V. is a registered non-profit organization that represents the [KDE Community](https://kde.org/) in legal and financial matters. <!-- (since 2023-10-11) -->
- **[R Foundation](https://www.r-project.org/foundation/donors.html#:~:text=Mehrad%20Mahmoudian)**: The R Foundation is a not for profit organization working in the public interest. It has been founded by the members of the R Development Core Team.



### Direct on and off donations

- **[The Internet Archive](https://archive.org/)**: Provides free access to collections of digitized materials including websites, software applications, music, audiovisual, and print materials<sup>[[source](https://en.wikipedia.org/wiki/Internet_Archive)]</sup>. The Archive also advocates for a free and open Internet. Their "mission is to provide Universal Access to All Knowledge". I personally am a big fan of their [Wayback Machine](https://archive.org/web/) which can archive a webpage  and be used as a time machine to access the webpage exactly as it was even after a decade.

-------


# Computational Donation

I started [contributing to the Folding@home project](https://stats.foldingathome.org/donor/16850) many years ago and quite liked it, but it was very limited to one project and there was a time that they didn't push any working unit, so I switched to Boinc and have contributed in many projects under quite a few different nicknames, but the following is the status of my contributions that are under my own name:

<!-- ![](https://boinc.mundayweb.com/stats.php?userID=15437&trans=off)
![My BOINC contributions](https://www.boincstats.com/signature/-1/user/3423704/sig.png) -->

<figure class="image is-flex is-justify-content-center is-align-items-center">
    <img src="https://www.boincstats.com/signature/-1/user/3423704/sig.png" style="max-width: 354px" alt="My BOINC contributions">
</figure>

<figure class="image is-flex is-justify-content-center is-align-items-center">
    <img src="https://signature.statseb.fr/sig-2784.png" style="max-width: 354px" alt="My BOINC badges">
</figure>


## Using Project Manager

My main focus for CPU donation is on [WorldCommunityGrid](https://www.worldcommunitygrid.org) which is maintained by IBM. My profile along with details of my contribution and the projects I have donated to can be found in [my profile](https://www.worldcommunitygrid.org/stat/viewMemberInfo.do?userName=m.mahmoudian).


| Project                            | Points Generated | Results Returned | Total Run Time (`y:d:h:m:s`) |
|:-----------------------------------|:-----------------|:-----------------|:-----------------------------|
| OpenPandemics - COVID-19           | 229,922          | 366              | 0:090:23:56:17               |
| Africa Rainfall Project            | 32,516           | 7                | 0:004:02:03:33               |
| Help Stop TB                       | 7,259            | 5                | 0:001:03:10:53               |
| Mapping Cancer Markers             | 27,691,199       | 35,452           | 14:358:09:17:20              |
| Microbiome Immunity Project        | 2,697            | 7                | 0:000:10:22:38               |
| OpenZika                           | 2,415,526        | 6,778            | 1:309:14:29:24               |
| FightAIDS@Home - Phase 2           | 3,802,781        | 4,368            | 1:359:16:11:34               |
| Outsmart Ebola Together            | 122,995          | 437              | 0:039:01:32:04               |
| Uncovering Genome Mysteries        | 3,165            | 5                | 0:000:12:25:53               |
| The Clean Energy Project - Phase 2 | 30,613           | 45               | 0:006:05:54:02               |
| FightAIDS@Home                     | 1,141,849        | 7,113            | 0:214:00:24:00               |
| Smash Childhood Cancer             | 1,035,429        | 2,598            | 0:154:17:14:38               |

<sub>Statistics By Project (Last Updated: 2023-09-16T15:33:33+03:00)</sub>

I also used to use the [Science United](https://scienceunited.org/) which is affiliated with [UC Berkley](https://www.berkeley.edu/) as BOINC project manager as backup to the World Community Grid (which has been [offline for quite some time](https://www.worldcommunitygrid.org/news/3)). Unfortunately the ScienceUnited is so poorly designed that it is practically abusing users by not giving the contributors any credit as it rename the contributor while relaying their contribution to the original project. Also the projects can easily disappear from the list of contributions that it is very hard to keep track. For this reason I have stopped contributing through the Science United and have switched to contributing directly to the projects I care about since 2022-06-13. Anyways, according to the dashboard, since 2022-01-17 until 2022-06-13 I have contributed:

| Project name           | CPU hours |
|:-----------------------|----------:|
| QuChemPedIA@home       | 12,960.85 |
| Climateprediction.net  | 12,841.70 |
| Universe@Home          | 5,576.55  |
| ODLK1                  | 4,000.92  |
| ODLK                   | 2,595.77  |
| Amicable Numbers       | 383.81    |
| PrimeGrid              | 327.97    |
| LHC@home               | 21.71     |


## Direct Projects

In the following table I have listed the projects that I'm directly contributing to without using project managers:

| Project                            | User ID | Cross-project ID                 | Points Generated |
|:-----------------------------------|:--------|:---------------------------------|:-----------------|
| climateprediction.net              | 2021044 | b23517f008d3ed92695c1152e6e3b680 | 143,095          |
| QuChemPedIA@home                   | 1582    | b23517f008d3ed92695c1152e6e3b680 | 141,200          |
| Rosetta@home                       | 566050  | b23517f008d3ed92695c1152e6e3b680 | 2,200,700        |

<sub>Statistics By Project (Last Updated: 2022-08-09T12:18:24+03:00)</sub>

See my stats live from [BOINC Combined Statistics](https://boinc.netsoft-online.com/e107_plugins/boinc/get_user.php?cpid=b23517f008d3ed92695c1152e6e3b680&html=1), [BOINCstats](https://www.boincstats.com/stats/-1/host/detail/57f569892bec65ac5d4bb963d4834c2e), or from [Free-DC](https://stats.free-dc.org/stats.php?page=hostbycpid&cpid=57f569892bec65ac5d4bb963d4834c2e). You can also see my badges as collections from [Signature for BOINC users](https://signature.statseb.fr/index.py?cpid=b23517f008d3ed92695c1152e6e3b680).


-------


# Community Contributions

Forums and message boards have been raised and perished during the years, so I only list those that I can link to:

<a href="https://stackexchange.com/users/1313171/mehrad-mahmoudian?tab=accounts">
  <img src="https://stackexchange.com/users/flair/1313171.png" width="208" height="58" alt="My profile on Stack Exchanges" title="My profile on Stack Exchanges">
</a>
