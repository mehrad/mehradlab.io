+++
title = ".: Mehrad :."
description = "Yet another nerdy figure roaming on the planet wondering why the answer was 42!"
+++

Bioinformatician🧬 during the day, Data Science📈 in the afternoon and Linux🐧 hobbyist at night. Fediverse advocate. Interested in Data Analysis, Machine Learning, and FOSS/FLOSS.

