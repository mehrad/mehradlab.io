+++
title="Emacs handy commands"
date=2022-04-28

[taxonomies]
categories = ["Lovely Linux", "Emacs"]
tags = ["post", "CLI", "Emacs", "IDE"]
+++

# Abstract

In this post I will briefly touch upon some of the handy commands that can help new Emacs users like myself to have a smoother transition to Emacs and easier adoption and integration of it in to their workflows.
The aim is NOT to teach Emacs, but it is to mention things that took me some times up to some hours of searching to find online.
I'll update this blog post every now and then to add more content instead of creating new blog posts.
This would make this blog post a one-page cheatsheet for myself and others.

<!-- more -->

# Variables

You can read the help/manual of each of these variables by `C-h v` and then typing the variable name (tab completion works).

## Personal Information

```el
(setq 
    user-full-name "John Doe"               ; The Emacs User's name (used by GPG)
    user-mail-address "john@doe.me"         ; The Emacs User's email (used by GPG)
    )
```

## Editor

```el
(setq
    undo-limit 80000000                     ; Raise undo-limit to 80Mb
    tab-width 4                             ; set the tab width
    tab-always-indent 'complete             ; make tab key do indent first then completion

    )
```

## Paths

```el
 (setq
     org-directory "~/org/"                 ; default location for org-mode files
     )
```

# Function

You can get help for each function by `C-h f` and then typing the function name (tab completion works).
You can execute these functions by `M-x` and then typing the function name.

## Editing

- `untabify`: Converts tabs to appropriate number of spaces (defined by `tab-width` variable)
- `tabify`: Converts spaces to appropriate number of tabs (defined by `tab-width` variable)
- `undo`: Normal undo
- `undo-redo`: Normal redo
- `read-only-mode`: Makes the current buffer read-only and can be triggered by `C-x C-q`. This is very handy when dealing with todo lists and READMEs.

## UI

- `text-scale-increase`: Increase the font size
- `text-scale-decrease`: Decrease the font size
