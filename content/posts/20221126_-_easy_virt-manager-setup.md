+++
title="Easy virt-manager setup"
date=2022-11-26

[taxonomies]
categories = ["Lovely Linux"]
tags = ["post", "virtualization", "ArchBTW"]
+++

# Abstract

In this short article I will explain how and what to install on an Arch-based distro to have a system to run some VMs. It is usually a controversial topic, but this is what I use. It does not mean it is the most correct way, but it means this setup has worked for me.

<!-- more -->

At this point of my Linux journey I don't care about non-rolling-release distros (e.g Ubuntu, CentOS, Fedora) as daily driver, but the general instructions should be extremely similar, so keep reading.

There are various ways to have such setup using various software stack. Some [honorable mentions at the end of this article](#honorable-mentions), but what I'm going to use here is the following combination as imho it is superior setup (for me personally):

`virt-manager` + `qemu`


# Setup

1. Check if your CPU supports virtualization:
```sh
LC_ALL=C lscpu | grep Virtualization
```
The acceptable value for the `virtualization` field should be:
- for Intel CPUs: `VT-x`
- for AMD CPUs: `AMD-V`


2. Install the following packages:
```sh
sudo pacman -S libvirt qemu-full virt-manager
```
Note that while installing them, there might be lots of optional dependencies. Go through them and install things you need for your setup. Qemu is amazing, but it also has lots and lots of components, so be patient and research what you need and install them. Of course you can later install them.

3. enable and start the libvirt daemon
```sh
systemctl enable --now libvirtd
```

Now, everything should be ready. you can run the `virt-manager` from your launcher or even terminal. Upon the first run, you should "add connection" to qemu. I have also seen that restarting once solves lots of issues for the first run, because there are some situation that in the first run the `virt-manager` cannot connect to the libvirt daemon.


# Honorable mentions 

- [Virtual Box](https://www.virtualbox.org): I have used it for many years back when I was a windows user or later when I was OSX or Linux user. It has a very intuitive UI and it is not easy to mess it up, but for the same reason it is limited. Also being Oracle product makes it less interesting for me.
