+++
title="Interesting F[L]OSS Games"
date=2023-03-31

[taxonomies]
categories = ["Lovely Linux", "FLOSS"]
tags = ["post", "will get updated"]
+++

# Abstract

In this post I'm going to list some of the Free and Libre OpenSource games that I have found interesting. 


<!-- more -->

Like it or not, there are three major things that have derived majority of the technologies we are using today: Wars, games and porn!
I don't think the former needs any explanation if you know just a tad bit about history.
As for the role of games, no one can ever deny the role of games on improvement of computers. Even before the cryptocurrency fad, games were what was pushing for better and faster hardware and software.
And of course, the role of porn is undeniable. From books and magazines, to film industry, to internet and etc. This of course is mostly because of the [Rule34](https://en.wikipedia.org/wiki/Rule_34) and all the "diversity" it brings to the table!

And all these bring us to the point that games are important.
They are important for children to develop their skills (motor, logic, etc.), and they are important to generate enough interest (financially and of other kinds) to certain technologies.
For instance, one of the claims of Windows user for long has been that they use windows because all games work on Windows and not on Linux.
Of course this statement is false, but up until few years back, most of the popular games were only playable on Windows simply because the devs of those games were/are ignorant enough not to make a binary for Linux.
Yes, there are other factors, but Linux was never interesting to the gaming industry and the devs went where the money was!
All these changed when Steam started working on [Proton](https://github.com/ValveSoftware/Proton/) which is a "compatibility tool for Steam Play based on Wine and additional components" so that most (if not all) Windows games can now run on Linux via this compatibility layer.
In addition to Proton, [Vulkan](https://www.vulkan.org/made-with-vulkan) also helped in making [OpenSource](https://github.com/KhronosGroup/Vulkan-Headers) cross-platform low-overhead 3d graphics API.
According to [protondb](https://www.protondb.com/) at the time of writing this article, there are 1888 game titles that are officially verified to work perfectly on Linux (specifically on Steam Deck), and 4040 titles have been officially considered as "playable" (perhaps with some small glitches).

Now that we have established some basics, Let's get to my list; but before that, I would list some of the **already existing lists** that are more comprehensive and long:

- <https://en.wikipedia.org/wiki/List_of_open-source_video_games>
- <https://project-awesome.org/leereilly/games>


# My list

## 2048

[[repo](https://melpa.org/#/2048-game)]

Of course being an Emacs user, I will bring this first :D .
It can be installed [via MELPA](https://melpa.org/#/2048-game).
I have linked the header to link to the git repo.


## OpenClonk

[[repo](https://github.com/openclonk/openclonk)] [[website](https://www.openclonk.org/)] [[ISC](https://opensource.org/license/isc-license-txt)]

A Terraria-like game (well, was written years before Terraria), but imho with better graphics.
The official website is which has some screenshot and even a video demoing the game.
I haven't played it yet, but I will!


## Mindustry

[[repo](https://github.com/Anuken/Mindustry)] [[website](https://mindustrygame.github.io/)] [[GPLv3](https://opensource.org/license/gpl-3-0)]

This is a strategy game describing itself as "a sandbox tower-defense game".
It is cross-platform and can be played on Linux, Android, and even web!
If I want to describe it, I should say it is a top-down single and multi-player strategy game that is a mixture of classic tower-defense with some [Factorio](https://www.factorio.com/).


## Xonotic

[[repo](https://gitlab.com/xonotic/xonotic)] [[website](https://xonotic.org/)] [[GPLv3](https://opensource.org/license/gpl-3-0)]

Xonotic is a cross-platform single and multiplayer first-person shooter with beautiful graphics and music.
It is a fast-paced game with multiple (if I remember correctly 6 game modes.
When I have few minutes to burn and not reading or watching anything, I will jump into a public server and enjoy the dynamics.


##  FlightGear Flight Simulator

[[repo](https://sourceforge.net/projects/flightgear/)] [[website](https://www.flightgear.org/)] [[GPLv2](https://opensource.org/license/gpl-2-0)]

As the name suggests, this is a flight simulator like most others, **but** it is FLOSS, cross-platform, and very actively developed.
The devs are apparently very enthusiastic about having all the models and physics as accurate as it can get, which I highly appreciate.
I haven't played it yet, but based on the pictures and [the video I watched](https://youtu.be/Dl756pvpiIA), it looks fascinating.

## Unvanquished

[[repo](https://github.com/Unvanquished/Unvanquished)] [[website](https://unvanquished.net)] [[GPLv3](https://opensource.org/license/gpl-3-0) (code) & [CC BY-SA](http://creativecommons.org/licenses/by-sa/2.5/) (media)]

A real-time strategy game played as a first-person shooter where evolving aliens and heavily armed humans fight for their survival.
This game as far as their Github repo suggests, is highly actively maintained.
It is a cross-platform first-person shooter game with some alien theme.
The devs describe the game as "a free, open-source first-person strategy game shooter, pitting technologically advanced human soldiers against hordes of highly adaptable aliens".
The major differece between Unvanquished and Xonotic is that each team has two major player roles: a builder and a fighter.
The builder can also fight, but not in the scale of the fighters.
Perhaps the best game to compare Unvanquished to would be Team Fortess 2 because the players can have different roles.
Although I would argue that Unvanquished is more advanced than Team Fortess 2, because you can change your weopons during the game and buy upgrades based on the scores you made in that match.
Unvanquished can be played on public servers or you can even setup a LAN server in a couple of clicks to play with your fiends and family.
There are many YouTube videos you can watch and see how the game looks and feels like, for example here is [one video](https://youtu.be/5Zq_kVlqt34).


## Red Eclipse

[[repo](https://github.com/redeclipse/base)] [[website](https://www.redeclipse.net/)] [[zlib](https://opensource.org/license/zlib) and [more](https://github.com/redeclipse/base/blob/master/doc/all-licenses.txt)]

This is a cross-platform first-person shooter game with blue vs. red team concept.
It is actively developed and it has good game mechanics.


## OpenTTD

[[repo](https://github.com/OpenTTD/OpenTTD)] [[website](https://www.openttd.org)] [[GPLv2](https://opensource.org/license/gpl-2-0)]

An open source simulation game based upon Transport Tycoon Deluxe.
It can be played in single-player mode or multi-player. 
I have not played it, but a person who I take seriously have seriously recommended this game.
There are many let's plays on YouTube and other platforms.


## OpenSpades

[[repo](https://github.com/yvt/openspades)] [[website](https://openspades.yvt.jp/)] [[GPLv3](https://opensource.org/license/gpl-3-0)]

An OpenSource voxel first-person shooter in multiplayer mode.
It can connect to a vanilla/pyspades/pysnip server.


## Minetest

[[repo](https://github.com/minetest/minetest)] [[website](https://www.minetest.net/)] [[LGPLv2.1](https://opensource.org/license/lgpl-2-1) and [more](https://github.com/minetest/minetest/blob/master/LICENSE.txt)]

This is perhaps the most popular and famous Minecraft-like game (I want to try to avoild "clone" because it it is heavily inspired by Minecrfat but truely has it's own identity and mods). Similar to Minecraft, it is an open-world sandbox voxel game, but in my opinion it has much more to offer due to it's modding system (based on Lua) and very active community.
