+++
title="What Emacs' package.el can learn from Neovim's packer.nvim"
date=2023-04-05

[taxonomies]
categories = ["Lovely Linux", "Emacs", "Neovim", "Opinion"]
tags = ["post", "Emacs", "Neovim"]
+++

# Abstract

I used vim and Neovim for about a decade, and then during the past two years I have switched to [Emacs](https://www.gnu.org/software/emacs) (initially [Doom Emacs](https://github.com/doomemacs/doomemacs), and now my own config). There are perhaps some things that I have brought with myself subconsciously from [neo]vim to my Emacs config. But in this article I want to briefly touch upon the thing that I wish Emacs, and especially [package.el](https://git.savannah.gnu.org/cgit/emacs.git/tree/lisp/emacs-lisp/package.el) can adopt/learn from [Neovim's Packer](https://github.com/wbthomason/packer.nvim).

<!-- more -->

There are some good things about `package.el`, but the following are what I can think of while writing this article:
1. **It does the damn job!** We all [hopefully] know that package manager's work is not easy. It should take care of installing and updating packages, and it should also allow the user to handle the orphan packages, and it should massively assist about dependency conflicts. I don't know how either of `Packer` or `package.el` are handling these cases under the hood though.
2. **It is fast and easy to use.** I don't want to bring negative examples, but package.el is very out of the way of the user and it does what it does efficiently. It also provides sufficient amount of information about the packages states right from Emacs.
3. **It is shipped with Emacs.**


All these said, there are few things that feels archaic about `package.el`, from how it looks, to how it behaves. Here are the things that I don't like about `package.el`:
1. **It is not by default asynchronous.** This would make Emacs unresponsive and the whole update process time-consuming.
2. **It does not show what are the changes.** Almost always each package update is based on some VCS (e.g git) commits, and some might also come with some change-log. It would be good to allow user to read these _before_ attempting to update a package.
3. **It does not help the user to shortlist things.** What I mean by this is that there are times that you are installing, removing, or updating packages, and more than one package is marked for that action. There is no particular view mode in `package.el` to just list these in a short-list and let the user ponder on them. The only time [that I know of] that the user is provided with a list is after `(package-menu-mark-upgrade)` and then `(package-menu-execute)`, and that is only to ask for a single yes or no!

Out of these, the 1st and 2nd items are those that `Packer` does much better than `package.el`. `Packer` performs the update in an asynchronous way, and shows the process of updating. It also shows the changes directly from the commit messages. As for the 3rd item, `Packer` (unlike `package.el`) does not provide a catalogue of packages that user can read and choose from. Therefore it does not necessarily "need" to have such feature, but the `package.el` actually do provide a catalogue, and user can mark multiple packages and then perform some operations on them. For this reason it is imho essential to have a view mode that looks like something like `dashboard.el` or `magit` to show the user what is going to be updated, what removed and what installed, and let the user remove items from the list before performing the operation. Of course I'm not saying that this should be the default behavior, but it should be a feature for the sake of user-friendliness and usability.

The following is the output of one of updates with `Packer`:


<pre style="font-family: monospace; color: #a1b7a8; background-color: #24283b">

               <span style="color: #94c5f0;">packer.nvim - finished in 3.313s</span>
 <span style="color: #c0caf5">━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━</span>
 <span style="color: #2e56bd">✓</span> <span style="color: #e39a5b">Updated nvim-lualine/lualine.nvim: e80465d..c28a742</span>
  URL: https://github.com/nvim-lualine/lualine.nvim
  <span style="color: #29ae98">Commits:</span>
    <span style="color: #b48eda">c28a742</span> Fix: `searchcount` error (#1004) (#1005) <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">9170434</span> added colours to qf extension to distinguish quickfix and location list (#933) <span style="color: #475a7d">(5 days ago)</span>
    <span style="color: #b48eda">0ddacf0</span> feat: allow specifying theme as function (#998) <span style="color: #475a7d">(5 days ago)</span>
    <span style="color: #b48eda">4bfc6bc</span> chore: autogen (vimdocs+formating) <span style="color: #475a7d">(5 days ago)</span>
    <span style="color: #b48eda">44a0fba</span> feat: add name+parent path option for component(filename) (#945) <span style="color: #475a7d">(5 days ago)</span>

 <span style="color: #2e56bd">✓</span> <span style="color: #e39a5b">Updated kyazdani42/nvim-tree.lua: 45400cd..d1410cb</span>
  URL: https://github.com/kyazdani42/nvim-tree.lua
  <span style="color: #29ae98">Commits:</span>
    <span style="color: #b48eda">d1410cb</span> docs: :help for api.node (#2106) <span style="color: #475a7d">(31 hours ago)</span>
    <span style="color: #b48eda">94e3b09</span> chore(deps): bump amannn/action-semantic-pull-request (#2107) <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">0ef3d46</span> feat(#1974): experimental.git.async see https://github.com/nvim-tree/nvim-tree.lua/issues/2104 (#2094) <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">7ad1c20</span> fix: `api.node.open.preview` should toggle directories (#2099) <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">0c9bfe7</span> feat(#2092): add api.node.navigate.open.next, prev (#2093) <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">7cd722f</span> ci: ensure PR subjects follow semantic commit spec (#2096) <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">3e9509e</span> fix(#2088): actions change dir enable false does not update tree root (#2095) <span style="color: #475a7d">(2 days ago)</span>

 <span style="color: #2e56bd">✓</span> <span style="color: #e39a5b">Updated kyazdani42/nvim-web-devicons: 267af2d..0568104</span>
  URL: https://github.com/kyazdani42/nvim-web-devicons
  <span style="color: #29ae98">Commits:</span>
    <span style="color: #b48eda">0568104</span> fix: use official icon for `nim` (#246) <span style="color: #475a7d">(3 days ago)</span>
    <span style="color: #b48eda">aa962cd</span> ci: ensure PR subjects follow semantic commit spec (#243) <span style="color: #475a7d">(3 days ago)</span>
    <span style="color: #b48eda">d92b3f4</span> chore: update pre-commit hooks (#244) <span style="color: #475a7d">(5 days ago)</span>

 <span style="color: #2e56bd">✓</span> <span style="color: #e39a5b">Updated nvim-treesitter/nvim-treesitter: 0927565..411e771</span>
  URL: https://github.com/nvim-treesitter/nvim-treesitter
  <span style="color: #29ae98">Commits:</span>
    <span style="color: #b48eda">411e771</span> Update parsers: sql <span style="color: #475a7d">(6 hours ago)</span>
    <span style="color: #b48eda">871f566</span> Update parsers: svelte, swift, vimdoc <span style="color: #475a7d">(30 hours ago)</span>
    <span style="color: #b48eda">0a20d1b</span> feat(ecma): use lua-match for jsdoc injections <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">9711139</span> docs: add a small note about local queries <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">6f5a4f6</span> feat: use `-bundle` to build parsers on macOS <span style="color: #475a7d">(2 days ago)</span>
    <span style="color: #b48eda">cbfa7ca</span> Update parsers: awk, tiger <span style="color: #475a7d">(3 days ago)</span>
    <span style="color: #b48eda">06075ec</span> fix(scala): Add missing locals definitions for scala <span style="color: #475a7d">(4 days ago)</span>
    <span style="color: #b48eda">ee1d618</span> Update parsers: vhs <span style="color: #475a7d">(4 days ago)</span>
    <span style="color: #b48eda">93fa5df</span> chore(help)!: renamed to vimdoc <span style="color: #475a7d">(4 days ago)</span>
    <span style="color: #b48eda">2f3113f</span> fix(ruby): then blocks should not be `@conditional`, just "then" <span style="color: #475a7d">(5 days ago)</span>
    <span style="color: #b48eda">d546bcd</span> highlights(sql): add `full`, `any` and `some` keywords <span style="color: #475a7d">(5 days ago)</span>

 <span style="color: #2e56bd">✓</span> <span style="color: #e39a5b">Updated neovim/nvim-lspconfig: c345220..7179a46</span>
  URL: https://github.com/neovim/nvim-lspconfig
  <span style="color: #29ae98">Commits:</span>
    <span style="color: #b48eda">7179a46</span> docs: update server_configurations.md skip-checks: true <span style="color: #475a7d">(4 hours ago)</span>
    <span style="color: #b48eda">ae3debc</span> fix(sqls): deprecate sqls suggest sqlls instead (#2544) <span style="color: #475a7d">(4 hours ago)</span>
    <span style="color: #b48eda">8dc63a4</span> docs: update server_configurations.md skip-checks: true <span style="color: #475a7d">(7 hours ago)</span>
    <span style="color: #b48eda">de114a6</span> fix(dafny): add default cmd for Dafny 4 (#2541) <span style="color: #475a7d">(7 hours ago)</span>
    <span style="color: #b48eda">0bc0c38</span> docs: update server_configurations.md skip-checks: true <span style="color: #475a7d">(4 days ago)</span>
    <span style="color: #b48eda">623be4b</span> docs(nil_ls): add a link to an example (#2540) <span style="color: #475a7d">(4 days ago)</span>
    <span style="color: #b48eda">8cbfc30</span> docs: fix typo <span style="color: #475a7d">(4 days ago)</span>
    <span style="color: #b48eda">94291c9</span> refactor: fix stylua lint error (#2535) <span style="color: #475a7d">(6 days ago)</span>
    <span style="color: #b48eda">6125158</span> ci: removed unused packages (#2534) <span style="color: #475a7d">(6 days ago)</span>


 Press <span style="color: #9ece6a">'q'</span> to quit
 Press <span style="color: #9ece6a">'&ltCR&gt'</span> to show more info
 Press <span style="color: #9ece6a">'d'</span> to show the diff
 Press <span style="color: #9ece6a">'r'</span> to revert an update

</pre>

As you can see, `Packer`'s output is very lean, clean, and clear, and it contains almost all the information the user might need. I personally would have liked to see a box dedicated to each package, so that the package dev could use to inform the user about a breaking change. But even in the current state it is good enough.

I do acknowledge that in Emacs-land, packages don't usually break your workflow (at least haven't happened to me yet), but like any other software out there, the possibility is there, and it feels like living on a thin sheet of ice when updating packages on my work machine. Additionally, such view would show the user how active the package developers are and the users can start caring more about the FLOSS devs and try to support them in various ways.
