
+++
title="Rollback Flatpak and Snap packages"
date=2022-03-23

[taxonomies]
categories = ["Lovely Linux"]
tags = ["post", "CLI"]
+++

# Abstract

In this short post I briefly explain how to rollback (i.e install previous version) of a software that you have installed using Flatpak on Snap. This is useful especially when the new version does not work as expected.

<!-- more -->

# A little bit of background

Here I explain the story behind how I faced this issue in the first place. It kinda is a documented here for historical reasons. You can save your time and jump to the next section.

I had a super busy day with multiple meetings, and as a sane person I updated all my software the day before just to save time and be sure things work.
I checked everything including my presentation, my RStudio server, all the result files, figures and plots, etc.
The only thing I didn't do functionality check for was Zoom which I had installed on via Flatpak on my desktop computer and on my laptop as backup.

It turned out that the latest Zoom simply does not work and exits without any proper error. This means that Zoom team screwed up and didn't properly tested their software's flatpak.

```sh
flatpak run --verbose us.zoom.Zoom
```

> ```
> F: No installations directory in /etc/flatpak/installations.d. Skipping
> F: Opening system flatpak installation at path /var/lib/flatpak
> F: Opening user flatpak installation at path /home/mehrad/.local/share/flatpak
> F: Opening user flatpak installation at path /home/mehrad/.local/share/flatpak
> F: Opening system flatpak installation at path /var/lib/flatpak
> F: Opening user flatpak installation at path /home/mehrad/.local/share/flatpak
> F: Opening system flatpak installation at path /var/lib/flatpak
> F: /var/lib/flatpak/runtime/org.freedesktop.Platform/x86_64/21.08/fcf3dbb56c117835216058acfa99ae280d713029f8d674e1b56e019a20ed082d/files/lib32 does not exist
> F: Cleaning up unused container id 1009358100
> F: Cleaning up per-app-ID state for us.zoom.Zoom
> F: Allocated instance id 1936174581
> F: Add defaults in dir /us/zoom/Zoom/
> F: Add locks in dir /us/zoom/Zoom/
> F: Allowing x11 access
> F: Allowing pulseaudio access
> F: Pulseaudio user configuration file '/home/mehrad/.config/pulse/client.conf': Error opening file /home/mehrad/.config/pulse/client.conf: No such file or directory
> F: Running 'bwrap --args 40 xdg-dbus-proxy --args=42'
> F: Running 'bwrap --args 40 zoom'
> ```

Now one day later by listing the commits of Zoom flatpak on Flathub it is visible that they realized the mistake and corrected it after about 6 hours:

```sh
flatpak remote-info --log flathub us.zoom.Zoom | head -n 20
```

> ```
>         ID: us.zoom.Zoom
>        Ref: app/us.zoom.Zoom/x86_64/stable
>       Arch: x86_64
>     Branch: stable
> Collection: org.flathub.Stable
>   Download: 134.3 kB
>  Installed: 159.7 kB
>    Runtime: org.freedesktop.Platform/x86_64/21.08
>        Sdk: org.freedesktop.Sdk/x86_64/21.08
> 
>     Commit: 972052877d97e307ba6ef722172d742331ef9f4dc868a31af8b7906d1f644237
>     Parent: c82bfaaa1e765d92037e64f496836cae99413f979945faef9ce20fe9f9a3bb03
>    Subject: Revert Zoom 5.10.0.2450 as it doesn't start (18b07075)
>       Date: 2022-03-22 11:47:13 +0000
>    History:
> 
>     Commit: c82bfaaa1e765d92037e64f496836cae99413f979945faef9ce20fe9f9a3bb03
>    Subject: Update zoom.tar.xz to 5.10.0.2450 (eb405f5c)
>       Date: 2022-03-22 05:54:16 +0000
> ```

This is the type of scenario in which one should immediately jump to rollback the faulty software instead of telling people that on Linux Zoom didn't work or got broke and in the process hurt the reputation of a good and stable operating system.

# The solution

The background story above is about Flatpak, but I will also provide solution for Snap packages:

## Flatpak

One can think of Flatpak as a version-controlled repository.
Every new release has a commit hash (similar to git), has a "subject" which is equivalent of git commit messages, hash of the parent commit, and finally a date/time tag.
So to get a specific version, you can list all the commits of that software:

```sh
flatpak remote-info --log flathub us.zoom.Zoom | less
```

once you have found the hash of the commit you want, you can use the following to roll back to an older version (the `sudo` is required by `flatpak`):

```sh
sudo flatpak update --commit=c82bfaaa1e765d92037e64f496836cae99413f979945faef9ce20fe9f9a3bb03 us.zoom.Zoom
```

of course all these can be easily automated into one single simple command:

```sh
sudo flatpak update --commit=$(flatpak remote-info --show-parent flathub us.zoom.Zoom) us.zoom.Zoom
```

## Snap

I don't have anything specific installed via snap, so I will demo using the `gtk-common-themes`.
you can find the full name of the packages installed via snap using

```sh
snap list
```

> ```
> Name               Version                     Rev    Tracking       Publisher   Notes
> bare               1.0                         5      latest/stable  canonical✓  base
> core18             20220309                    2344   latest/stable  canonical✓  base
> gnome-3-28-1804    3.28.0-19-g98f9e67.98f9e67  161    latest/stable  canonical✓  -
> gtk-common-themes  0.1-59-g7bca6ae             1519   latest/stable  canonical✓  -
> snapd              2.54.4                      15177  latest/stable  canonical✓  snapd
> ```

Then you can see all the versions you have on your computer of a particular package.
For instance I have two versions of `gtk-common-themes`, one of which (the old one) is disabled:

```sh
snap list gtk-common-themes --all
```

> ```
> Name               Version          Rev   Tracking       Publisher   Notes
> gtk-common-themes  0.1-52-gb92ac40  1515  latest/stable  canonical✓  disabled
> gtk-common-themes  0.1-59-g7bca6ae  1519  latest/stable  canonical✓  -
> ```

Then you can easily revert to the previous version:

```sh
sudo snap revert gtk-common-themes
```

You can also revert back to specific revision number. the revision numbers are listed under the `Rev` column in the output above.

```sh
sudo snap revert gtk-common-themes --revision 1515
```
