+++
title="Flatpak tips"
date=2022-07-26

[taxonomies]
categories = ["Lovely Linux"]
tags = ["post", "CLI"]
+++

# Abstract

In this post I list some tips to help maintain your Flatpaks and have them in control.
In general Flatpaks are much easier and straightforward to use compared to Snap (and from personal experience faster), but still there are few things that are good to have in mind when using them.

<!-- more -->

# A little bit of background


# Tips

I will add more to this list whenever I face an issue to solve it


## Adding remotes

One of the advantages of Flatpak compared to Snap is that the user has the freedom to choose and add the repositories.
The most popular software repository is of course Flathub.org which is managed by the Flatpak team, but there are other Flatpak repositories out there (e.g [KDE flathub](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak)).
I list few here, but I don't endorse any one of them.
Make sure you check their integrity yourself.
Also remember that Gnome has already spawned and killed some Flatpak repos, so I personally would not put my faith into having this one around for long either. 

```sh
# Official Flathub
flatpak remote-add \
    --user \
    --if-not-exists flathub \
    --from https://flathub.org/repo/flathub.flatpakrepo

# KDE Plasma Flatpak
flatpak remote-add \
    --user \
    --if-not-exists kdeapps \
    --from https://distribute.kde.org/kdeapps.flatpakrepo

# Gnome nightly
flatpak remote-add \
    --user \
    --if-not-exists kdeapps \
    --from http://nightly.gnome.org/gnome-nightly.flatpakrepo
```


## Configuring permissions for each Flatpak

There is a fantastic software out there named [Flatseal](https://github.com/tchx84/flatseal) which is "a graphical utility to review and modify permissions from your Flatpak applications".
It lets you to define global permission settings and also modify settings and permissions of every Flatpak you have in your computer.
For instance, for a music player, there is no excuse for it to be bale to access network or printer.
In my personal opinion if a Flatpak has already enabled some permission that it doesn't/shouldn't logically need, that Flatpak is suspicions and I would either remove it, or restrict it as tight as possible and even run it in [Firejai](https://wiki.archlinux.org/title/Firejail).

One of the interesting things about Flatseal is that it is [also distributed as a Flatpak](https://flathub.org/apps/details/com.github.tchx84.Flatseal) :D
Of course you can (if available) install it via your Linux package manager as well.
[Check here](https://repology.org/project/flatseal/versions) to see if it is available.


## Removing unused dependencies

In Flatpak world, in order to avoid having the same runtime packaged many many times, some stuff are packaged independently and are installed as dependencies of some Flatpaks.
For instance the Nvidia stuff of GTK stuff are pretty massive (former is typically around 330 MBs) and various Flatpaks use different versions of them.
When your update your Flatpaks (`flatpak update`), typically the software move to new version of these dependencies as they get maintained every version.
This means that after using Flatpaks for half a years, you might end up having some dependencies that are not used (a.k.a orphaned) and are just taking extra space on your disks.
Fortunately, Flatpak has a builtin feature to detect and remove these unused dependencies:

```sh
flatpak uninstall --unused
```

This will check what is `unused` on your system and will prompt you to remove them. If you say yes (`y`), you will some some output like the following:

> ```
>         ID                                                      Branch           Op
>  1. [-] org.freedesktop.Platform.GL.nvidia-510-68-02            1.4              r
>  2. [-] org.freedesktop.Platform.GL.nvidia-510-73-05            1.4              r
>  3. [-] org.freedesktop.Platform.GL.nvidia-515-48-07            1.4              r
> 
> Uninstall complete.
> 
> 
>         ID                                                      Branch                Op
>  1. [-] org.freedesktop.Platform.GL.nvidia-510-68-02            1.4                   r
>  2. [-] org.freedesktop.Platform.GL.nvidia-510-73-05            1.4                   r
>  3. [-] org.freedesktop.Platform.GL.nvidia-515-48-07            1.4                   r
>  4. [-] org.kde.KStyle.Adwaita                                  5.15-21.08            r
>  5. [-] org.kde.Platform                                        5.15-21.08            r
>  6. [-] org.kde.Platform.Locale                                 5.15-21.08            r
> 
> Uninstall complete.
> ```
