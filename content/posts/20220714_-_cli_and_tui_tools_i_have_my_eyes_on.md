+++
title="CLI/TUI Tools I Have My Eyes on"
date=2022-07-14

[taxonomies]
categories = ["Lovely Linux"]
tags = ["post", "CLI", "will get updated"]
+++

# Abstract

This is not an article but rather a list of the software/tools I have found interesting either from feature aspect or design.
These software are under my radar and are listed here as a shortlist for my own future reference and potentially introduce them to those who checkout my website. 

<!-- more -->

The software here will not contain Emacs or Neovim, nor it will contain ohmyzsh type of project, although I find them super interesting.
This list will also potentially gets updated gradually. Therefore, if you are interested, visit this page every now and then to see the updates.

## GNU Utils alternative

These are software that are on-par or even improved version of [GNU Coreutils](https://savannah.gnu.org/projects/coreutils):


- [bat](https://github.com/sharkdp/bat): Modern `cat` [written in Rust]
- [exa](https://github.com/ogham/exa): Modern `ls` [written in Rust]
- [ripgrep](https://github.com/BurntSushi/ripgrep): More than `grep`. It is direct alternative to the [silversearcher](https://github.com/ggreer/the_silver_searcher)(`ag`), but much faster and much better [written in Rust]
- [fd](https://github.com/sharkdp/fd): Modern `find` with better syntax, default regex on, and multi-threaded [written in Rust]
- [viddy](https://github.com/sachaos/viddy): Modern `watch` [written in Go]
 

## Others

These are tools that are bringing new concepts/tools to the CLI/TUI tool-chain.

- [lazygit](https://github.com/jesseduffield/lazygit): TUI git interface [written in Go]
- [bottom](https://github.com/ClementTsang/bottom): system monitor similar to `bpytop`, `atop` and etc. [written in Rust]
- [entr](https://github.com/eradman/entr): Stands for "Event Notify Test Runner" and runs arbitrary commands when files change [written in C]
- [fzf](https://github.com/junegunn/fzf): general purpose command-line fuzzy finder [written in Go]
- [hyperfine](https://github.com/sharkdp/hyperfine): Benchmark performance and runtime of CLI tools size by side with proper statistics [written in Rust]
- [micro](https://github.com/zyedidia/micro): A better take on TUI text editors than `nano` or `vim` as you can interact with the editor like most GUI editors (mouse, Ctrl-c to copy, Ctrl-v to paste, hold shift and arrow to select, and etc.) [written in Go]
