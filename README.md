<p align="center">
  <img src="static/images/avatar.png" alt="Logo" width="80" height="80">

  <h3 align="center">Mehrad's Personal Website</h3>
</p>

<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#built-with">Built With</a></li>
  </ol>
</details>



## About The Project

This is my personal website. I don't think more needs to be told in this section :)

## Contributing

I might have typos, mistakes in the text of the website or come of my configs. Any contributions you make are **greatly appreciated**. Of course your name will end up in the text you have corrected/contributed.

Here is how to contribute:

1. Fork the Project
2. Clone it to your local computer (`git clone git@gitlab.com:mehrad/mehrad.gitlab.io.git` of course change `:mehrad/` to your account name)
2. Create your own Branch (`git checkout -b Correction`)
3. make changes to the files based on your preference
4. Commit your Changes. Something like one of the following:
    - `git commit -m '[fix] a typo was fixed'`
    - `git commit -m '[add] supportive link was added'`
5. Push to the Branch (`git push origin feature/AmazingFeature`)
6. Open a Pull Request


## License

This is my personal website, so of course everything here is considered as my intellectual property unless I have specified otherwise. Please becivilized and don't copy the text and publish it under your name.

## Contact

You can find the contact information on the website itself: https://mehrad.ai

## Built with

* [DeepThought theme](https://github.com/RatanShreshtha/DeepThought)
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Choose an Open Source License](https://choosealicense.com)
* [Slick Carousel](https://kenwheeler.github.io/slick)
* [Font Awesome](https://fontawesome.com)
* [Unsplash](https://unsplash.com/)
* [Zola](https://www.getzola.org/)
* [Bulma](https://bulma.io/)
